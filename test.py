import pandas as pd

class PointFour:
    def __init__(self) -> None:
        self.menu_show = f"""
        Digite el número correspondiente a la acción que desea realizar y presione Enter
            1. Añadir Proyecto
            2. Listar proyectos
            3. Buscar proyecto por ID
            4. Editar proyecto
            5. Desactivar proyecto
            6. Salir
        """
        self.user_select = ""
        self.dataframe = pd.DataFrame(columns = ["id", "nombre_proyecto", "encargado", "descripcion", "activo"])

    def show_menu(self) -> None:
        try:
            print(self.menu_show)
            self.user_select = int(input(''))
        except Exception as e:
            print("Ha seleccionado una opción no valida, digite nuevamente el número de la acción a realizar")
            self.show_menu()

    def get_user_select(self) -> str:
        return self.user_select

    def execute_action(self) -> None:
        if self.user_select == 1:
            self.__action_one()
        elif self.user_select == 2:
            self.__action_two()
        elif self.user_select == 3:
            self.__find_by_id()
        elif self.user_select == 4:
            self.__edit_by_id()
        elif self.user_select == 5:
            self.__inactivate_project()
        elif self.user_select == 6:
            quit()

    def __action_one(self) -> None:
        print('Digite el nombre del proyecto a almacenar')
        name = input('')
        while name.replace(' ', '') == '':
            print('Debe digitar un nombre valido')
            name = input('')
        print('Digite el Encargado del proyecto')
        encargado = input('')
        while encargado.replace(' ', '') == '':
            print('Debe digitar un encargado valido')
            encargado = input('')
        print('Digite una descripción si desea')
        descripcion = input('')
        while descripcion.replace(' ', '') == '':
            print('Debe digitar una descripcion valida')
            descripcion = input('')
        print("""Digite el numero correspondiente a alguna de las siguientes opciones. 
        En caso de dejar en blanco la opción por defecto es 1""")
        print("""
            1. Activo
            0. Inactivo
            """)
        active = input('')
        while active != '1' and active !='0':
            if active.replace(" ", "") == '':
                active = '1'
                break
            print('Digite un numero valido para el estado de activo o inactivo')
            active = input('')
        id_project = 1
        if self.dataframe.shape[0] > 0:
            id_project = int(self.dataframe.iloc[[-1]]['id'])+1
        self.dataframe = self.dataframe.append({"id": str(id_project), "nombre_proyecto": name, "encargado": encargado, "descripcion":descripcion, "activo": active}, ignore_index=True)

    def __action_two(self) -> None:
        print(self.dataframe)

    def __find_by_id(self) -> None:
        print('Digite el id a buscar')
        find_id = input('')
        data_original = self.dataframe.loc[self.dataframe['id'].isin([find_id])]
        if data_original.empty:
            print('No se ha encontrado el id del proyecto')
            return
        print(data_original)

    def __edit_by_id(self) -> None:
        print('Digite el (id) del proyecto editar')
        find_id = input('')
        data_original = self.dataframe.loc[self.dataframe['id'].isin([find_id])].copy()
        if data_original.empty:
            print('No se ha encontrado el id del proyecto')
            return
        print("\nProyecto a Editar:\n")
        print(data_original)
        print('\nLos campos que puede editar son los siguientes:')
        print('1.Nombre Proyecto, 2.Encargado, 3.Descripcion, 4. Activo, 5. Todos')
        print('Digite los numeros de los campos que desea editar separados por una coma(,) para editar determinadas columnas o 5 para todos, como el siguiente ejemplo')
        print('Especificar columnas:')
        print('1, 3')
        print('Todos')
        print('5')
        try:
            columns = input('').replace(' ', '').split(',')
        except Exception as e:
            print('**************Formato incorrecto, se le mostrará la información nuevamente******')
            self.__edit_by_id()
        for index, column in enumerate(columns):
            if column == '1' or column == '5':
                print('Digite el nuevo Nombre del proyecto')
                new_name = input('')
                while new_name.replace(' ', '') == '':
                    print('Debe digitar un nombre valido')
                    new_name = input('')
                self.dataframe['nombre_proyecto'].loc[self.dataframe['id'].isin([find_id])] = new_name
            if column == '2' or column == '5':
                print('Digite el nuevo Encargado del proyecto')
                new_encargado = input('')
                while new_encargado.replace(' ', '') == '':
                    print('Debe digitar un Encargado valido')
                    new_encargado = input('')
                self.dataframe['encargado'].loc[self.dataframe['id'].isin([find_id])] = new_encargado
            if column == '3' or column == '5':
                print('Digite la nueva descripcion')
                new_descripcion = input('')
                while new_descripcion.replace(' ', '') == '':
                    print('Debe digitar una Descipcion valida')
                    new_descripcion = input('')
                self.dataframe['descripcion'].loc[self.dataframe['id'].isin([find_id])] = new_descripcion
            if column == '4' or column == '5':
                print('Digite el nuevo estado')
                new_estado = input('')
                while (new_estado.replace(' ', '') != '1' and new_estado.replace(' ', '') != '0'):
                    print('Debe digitar un Estado valido')
                    new_estado = input('')
                self.dataframe['activo'].loc[self.dataframe['id'].isin([find_id])] = new_estado
        print('Datos Actualizados: ')
        print(self.dataframe)

    def __inactivate_project(self) -> None:
        print('Digite el (id) del proyecto editar')
        find_id = input('')
        data_original = self.dataframe.loc[self.dataframe['id'].isin([find_id])].copy()
        if data_original.empty:
            print('No se ha encontrado el id del proyecto')
            return
        self.dataframe['activo'].loc[self.dataframe['id'].isin([find_id])] = '0'
point_four = PointFour()
while (point_four.get_user_select() != 6):
    point_four.show_menu()
    point_four.execute_action()