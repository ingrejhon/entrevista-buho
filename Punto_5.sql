create table comercial(
	id bigserial not null primary key,
	nombre varchar(100) not null,
	apellido1 varchar(100) not null,
	apellido2 varchar(100) null,
	ciudad varchar(100) not null,
	comision float not null default 0.0
);

create table cliente(
	id bigserial not null primary key,
	nombre varchar(100) not null,
	apellido1 varchar(100) not null,
	apellido2 varchar(100) null,
	ciudad varchar(100),
	categoria integer not null
);

create table pedido(
	id bigserial not null primary key,
	cantidad double precision not null default 0.0,
	fecha date default now(),
	id_cliente bigint not null,
	id_comercial bigint not null,
	constraint fk_pedido_id_cliente
	foreign key(id_cliente)
	references cliente(id),
	constraint fk_pedido_id_comercial
	foreign key(id_comercial)
	references comercial(id)
);

insert into comercial(
	nombre, apellido1, 
	apellido2, ciudad, 
	comision
)values (
	'Diana', 'Gutierrez',
	'Vasquez', 'Armenia',
	'0.03'
), (
	'Pedro', 'Perez',
	'Torres', 'Bogot�',
	'0.15'
), (
	'Juan', 'Lopez',
	'Casta�eda', 'Bogot�',
	'0.2'
), (
	'Camila', 'Garcia',
	'Pineda', 'Bogot�',
	'0.11'
), (
	'Antonio', 'Suares',
	'Garcia', 'Medell�n',
	'0.14'
), (
	'Javier', 'Pe�a',
	'Orozco', 'Medell�n',
	'0.45'
), (
	'Luisa', 'Mejia',
	'Duarte', 'Bogot�',
	'0.21'
), (
	'Maria', 'Campos',
	'Z�rate', 'Popay�n',
	'0.05'
);

insert into cliente(
	nombre, apellido1,
	apellido2, ciudad,
	categoria 
) values(
	'Robinson', 'Arismendy',
	'Cardenas', 'Armenia',
	'1'
), (
	'Michael', 'Hortua',
	'Dominguez', 'Bogot�',
	'1'
), (
	'Adriana', 'Uribe',
	'Cesar', 'Bogot�',
	'1'
), (
	'Ximena', 'Velazquez',
	'Rojas', 'Bogot�',
	'2'
), (
	'Fredy', 'Martin',
	'Garcia', 'Medell�n',
	'2'
), (
	'Wilson', 'Jimenez',
	'Sabina', 'Medell�n',
	'1'
), (
	'Dylan', 'Grijalba',
	'Lopez', 'Bogot�',
	'3'
), (
	'Frank', 'Torres',
	'Rocha', 'Popay�n',
	'2'
);

insert into pedido (
	cantidad, fecha,
	id_cliente, id_comercial 
) values(
	'345', '2022-01-04',
	'2', '1'
), (
	'445', '2022-02-03',
	'4', '6'
), (
	'234', '2022-02-15',
	'4', '4'
), (
	'122', '2022-03-20',
	'2', '7'
), (
	'10.4', '2022-03-21',
	'2', '7'
), (
	'182.3', '2022-05-15',
	'6', '3'
), (
	'332.2', '2022-06-01',
	'5', '4'
), (
	'446', '2022-06-03',
	'4', '2'
), (
	'456', '2022-06-03',
	'2', '3'
), (
	'23.4', '2022-06-03',
	'2', '7'
), (
	'234', '2022-06-03',
	'2', '8'
);

------------------- Consultas de Puntos -----------------
---- Devuelve todos los datos de los dos pedidos de mayor valor.
select *
from pedido p
order by p.cantidad desc
limit 2;

/* 
 * Devuelve un listado con el nombre y los apellidos de los comerciales que
 * tienen una comisi�n entre 0.05 y 0.11.
 */
select c.nombre, concat(c.apellido1, ' ', c.apellido2) as apellidos 
from comercial c 
where c.comision between '0.05' and '0.11';

/*
 * Devuelve el nombre de todos los clientes que han realizado alg�n pedido
 * con el comercial Luisa Mejia Duarte.
*/
select c.*
from cliente c 
inner join pedido p on p.id_cliente = c.id 
where p.id_comercial = (
	select id
	from comercial c3 
	where c3.nombre = 'Luisa' and c3.apellido1 = 'Mejia'
	and c3.apellido2 = 'Duarte'
);

/*
 * Devuelve un listado con todos los clientes junto con los datos de los pedidos
 * que han realizado. Este listado tambi�n debe incluir los clientes que no han
 * realizado ning�n pedido. El listado debe estar ordenado alfab�ticamente
 * por el primer apellido de los clientes.
*/

select c.*, p.*
from cliente c
left join pedido p on p.id_cliente = c.id 
order by c.apellido1 desc;

/*
 * Calcula cu�l es el m�ximo valor de los pedidos realizados durante el mismo
 * d�a para cada uno de los clientes
*/
select c.*, (
	select max(p.cantidad)
	from pedido p
	where p.id_cliente = c.id
) as maximo_valor_pedido
from cliente c ;

/*
 * Devuelve el n�mero total de pedidos que se han realizado cada a�o
*/
SELECT date_trunc('year', fecha) AS by_year, sum(cantidad) as sum_year
FROM pedido
GROUP BY by_year;